#!/bin/bash

cf=$1
ip=$2
echo "The following shares have successfully been mounted" > "${ip}/successfulShares.txt"
mkdir "${ip}"
smbclient -L "//${ip}" -A "${cf}" | grep -i "disk \| ipc" | sed 's/|/ /' | awk '{print $1 $8}'|
while read share; do
    cfPath=$(readlink -f ${cf})
    shareLocation="//${ip}/${share}"
    echo $shareLocation
    mountDir="${ip}/${share}"
    mkdir "${mountDir}"
    mount -v -t cifs -o credentials="${cfPath}" ${shareLocation} "${mountDir}"
    find "${mountDir}" > "${ip}/findResults.txt"
done
