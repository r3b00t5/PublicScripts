To use multiMount.sh:

1. download multiMount.sh, mountShare.sh, and credentials.txt.
2. Place them all in the same directory
3. Configure credentials.txt as such

```
username=
password=
domain=
```

4. Run multiMount.sh

```
multiMount.sh -c credentials.txt [-i <Single IP>][-f <IP ListFile>]
```