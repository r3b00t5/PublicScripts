#!/bin/bash

while getopts c:i:f:h option
do
    case "${option}"
    in
        # Mount compatible credential file
        c) credentials=${OPTARG};;

	# Single target IP
        i) target=${OPTARG}
        ./mountShare.sh ${credentials} ${target};;

	# Multi target IP list
	f) targetList=${OPTARG}
	for targetIP in $(cat $targetList)
	do
	    ./mountShare.sh ${credentials} ${targetIP}
	done;;
	h) echo "Specify a single target with -i or a file that contains a list of targets with -f. -h displays this message" ;;
	*) echo "Specify a single target with -i or a file that contains a list of targets with -f. -h displays this message" ;;
	esac
done